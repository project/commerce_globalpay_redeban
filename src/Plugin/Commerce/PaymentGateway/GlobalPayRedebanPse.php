<?php

namespace Drupal\commerce_globalpay_redeban\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_checkout\CheckoutOrderManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\commerce_globalpay_redeban\GlobalPayRedebanUtilitiesInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\Core\Discovery\YamlDiscovery;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "globalpay_redeban_pse",
 *   label = "GlobalPay Redeban PSE",
 *   display_label = "PSE",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_globalpay_redeban\PluginForm\OffsiteRedirect\PseForm",
 *   },
 *   payment_method_types = {"globalpay_redeban"},
 *   requires_billing_information = FALSE,
 * )
 */
class GlobalPayRedebanPse extends OffsitePaymentGatewayBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal renderer service.
   *
   * @var Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The checkout order manager.
   *
   * @var \Drupal\commerce_checkout\CheckoutOrderManagerInterface
   */
  protected $checkoutOrderManager;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The GlobalPay Utilities.
   *
   * @var \Drupal\commerce_globalpay_redeban\GlobalPayRedebanUtilitiesInterface
   */
  protected $globalPayUtilities;


  /**
   * The collection of Payu oficial Images.
   *
   * @var array
   */
  protected $images;

  /**
   * The configuration of the module.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Constructs a new PayuWebcheckout object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Drupal renderer service.
   * @param \Drupal\commerce_checkout\CheckoutOrderManagerInterface $checkout_order_manager
   *   The checkout order manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\commerce_globalpay_redeban\GlobalPayRedebanUtilitiesInterface $globalpay_utilities
   *   The globalpay utilities.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager $payment_type_manager,
    PaymentMethodTypeManager $payment_method_type_manager,
    TimeInterface $time,
    ModuleHandlerInterface $module_handler,
    RendererInterface $renderer,
    CheckoutOrderManagerInterface $checkout_order_manager,
    RouteMatchInterface $route_match,
    GlobalPayRedebanUtilitiesInterface $globalpay_utilities,
    ConfigFactoryInterface $config_factory
  ) {
    $this->moduleHandler = $module_handler;
    $this->renderer = $renderer;
    $this->checkoutOrderManager = $checkout_order_manager;
    $this->routeMatch = $route_match;
    $this->globalPayUtilities = $globalpay_utilities;
    $this->config = $config_factory->get('commerce_globalpay_redeban.globalpay_settings');
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_type_manager,
      $payment_type_manager,
      $payment_method_type_manager,
      $time
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('module_handler'),
      $container->get('renderer'),
      $container->get('commerce_checkout.checkout_order_manager'),
      $container->get('current_route_match'),
      $container->get('commerce_globalpay_redeban.utilities'),
      $container->get('config.factory')
    );
  }

  /**
   * Obtains Images set in yaml.
   *
   * @return array
   *   An array with images found in Yaml files
   *   whose keys are the image key and whose
   *   value, is the actual image path to use.
   */
  protected function pseImages() {
    if ($this->images) {
      return $this->images;
    }
    $discovery = new YamlDiscovery('globalpay_images', $this->moduleHandler->getModuleDirectories());
    $definitions = $discovery->findAll();
    $this->images = [];
    foreach ($definitions as $definition) {
      foreach ($definition as $key => $image) {
        $this->images[$key] = $image;
      }
    }
    return $this->pseImages();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'image' => '',
      'api_login' => 'Application Code Server',
      'api_key' => 'Application Key Server',
      'endpoint_banks' => 'https://noccapi-stg.globalpay.com.co/banks/',
      'endpoint_create_transaction' => 'https://noccapi-stg.globalpay.com.co/order/',
      'endpoint_check_transaction' => 'https://noccapi-stg.globalpay.com.co/',
      'redirect_method' => 'post_manual',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $image_options = [];
    foreach (array_keys($this->pseImages()) as $image_key) {
      $image_options[$image_key] = ucfirst(str_replace('_', ' ', $image_key));
    }

    $form['image_key'] = [
      '#type' => 'select',
      '#title' => $this->t('Payment selection image.'),
      '#description' => $this->t('Select the image you would like to have displayed at the payment method pane. The codes you see listed are taken from the official PayU Corporative logo guidelines available <a href="@link">here</a>.', ['@link' => 'http://www.payulatam.com/logos/index.php']),
      '#options' => $image_options,
      '#default_value' => $this->configuration['image_key'],
      '#empty_option' => $this->t('No image'),
      '#empty_value' => 'no_image',
    ];

    $form['api_login'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('API Login'),
      '#default_value' => $this->configuration['api_login'],
      '#description' => $this->t('Application Code Server'),
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('API Key'),
      '#default_value' => $this->configuration['api_key'],
      '#description' => $this->t('Application Key Server'),
    ];

    $form['endpoint_banks'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Endpoint Banks'),
      '#default_value' => $this->configuration['endpoint_banks'],
      '#description' => $this->t('Endpoint URL, for example: https://noccapi-stg.globalpay.com.co/banks/ (Do not include <carrier> parameter).'),
    ];

    $form['endpoint_create_transaction'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Endpoint Create Transactions'),
      '#default_value' => $this->configuration['endpoint_create_transaction'],
      '#description' => $this->t('Endpoint URL, for example: https://noccapi-stg.globalpay.com.co/order/.'),
    ];

    $form['endpoint_check_transaction'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Endpoint Check Transactions'),
      '#default_value' => $this->configuration['endpoint_check_transaction'],
      '#description' => $this->t('Endpoint URL, for example: https://noccapi-stg.globalpay.com.co/.'),
    ];

    $form['redirect_method'] = [
      '#type' => 'hidden',
      '#default_value' => 'post',
      '#default_value' => $this->configuration['redirect_method'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $images = $this->pseImages();
      $this->configuration['image_key'] = $values['image_key'];
      $this->configuration['image'] = isset($images[$values['image_key']]) ? $images[$values['image_key']] : NULL;
      $this->configuration['api_login'] = $values['api_login'];
      $this->configuration['api_key'] = $values['api_key'];
      $this->configuration['endpoint_banks'] = $values['endpoint_banks'];
      $this->configuration['endpoint_create_transaction'] = $values['endpoint_create_transaction'];
      $this->configuration['endpoint_check_transaction'] = $values['endpoint_check_transaction'];
      $this->configuration['redirect_method'] = $values['redirect_method'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $hash = $this->globalPayUtilities->generateHash($this->configuration['api_login'], $this->configuration['api_key']);
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payments = $payment_storage->loadMultipleByOrder($order);
    foreach ($payments as $payment) {
      if ($payment->getState()->value == 'pending' && $payment->getPaymentGatewayId() == $this->entityId && $payment->getRemoteId()) {
        $response = $this->globalPayUtilities->checkBankTransfer($this->configuration['endpoint_check_transaction'], 'pse', $payment->getRemoteId(), $hash);
        if (!empty($response['transaction']['status']) && $response['transaction']['status'] == 'approved') {
          $payment->setState('completed');
          $payment->setRemoteState($response['transaction']['status']);
          $payment->save();
        }
        elseif (empty($response['transaction']['status']) || $response['transaction']['status'] == 'pending') {
          $expireTime = !empty($this->config->get('minutes_between_validations')) ? $this->config->get('minutes_between_validations') : 5;
          // Before was 30 seconds ... corrected to 60 seconds.
          $expireTime *= 60;
          $payment->setState('pending');
          $time = REQUEST_TIME + $expireTime;
          $payment->setExpiresTime($time);
          $payment->setRemoteState($response['transaction']['status']);
          $payment->save();
        }
        else {
          $payment->setState('voided');
          $payment->setRemoteState($response['transaction']['status']);
          $payment->save();
          throw new PaymentGatewayException('The transaction has been rejected.');
        }
      }
    }
  }

}

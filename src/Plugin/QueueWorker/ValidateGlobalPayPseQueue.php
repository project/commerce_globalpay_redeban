<?php

namespace Drupal\commerce_globalpay_redeban\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_globalpay_redeban\GlobalPayRedebanUtilitiesInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Processes packages and recharges.
 *
 * @QueueWorker(
 *   id = "validate_globalpay_pse_queue",
 *   title = @Translation("Validation GlobalPay PSE Queue"),
 *   cron = {"time" = 30}
 * )
 */
class ValidateGlobalPayPseQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The WS Service.
   *
   * @var \Drupal\commerce_globalpay_redeban\GlobalPayRedebanUtilitiesInterface
   */
  protected $globalPayUtilities;

  /**
   * The configuration of the module.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The function that create the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('commerce_globalpay_redeban.utilities'),
      $container->get('config.factory')
    );
  }

  /**
   * Overrides \Drupal\Component\Plugin\PluginBase::__construct().
   *
   * Overrides the construction of context aware plugins to allow for
   * unvalidated constructor based injection of contexts.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_globalpay_redeban\GlobalPayRedebanUtilitiesInterface $globalPayUtilities
   *   The globalpay utilities.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    GlobalPayRedebanUtilitiesInterface $globalPayUtilities,
    ConfigFactoryInterface $config_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->globalPayUtilities = $globalPayUtilities;
    $this->config = $config_factory->get('commerce_globalpay_redeban.globalpay_settings');
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($item) {
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->load($item->payment_id);
    if ($payment->getState()->value == 'pending' && $payment->getRemoteId()) {
      $payment_gateway = $payment->getPaymentGateway();
      $configuration = $payment_gateway->getPluginConfiguration();
      if (!empty($configuration['mode']) && $configuration['mode'] == 'test') {
        $response = $this->testBankTransfer();
      }
      else {
        $hash = $this->globalPayUtilities->generateHash($configuration['api_login'], $configuration['api_key']);
        $response = $this->globalPayUtilities->checkBankTransfer($configuration['endpoint_check_transaction'], 'pse', $payment->getRemoteId(), $hash);
      }
      if (!empty($response['transaction']['status']) && $response['transaction']['status'] == 'approved') {
        $payment->setState('completed');
        $payment->setRemoteState($response['transaction']['status']);
        $payment->save();

        // Add place transition to order to prevent draft orders after queue
        // validation.
        $order = $payment->getOrder();
        if (!$order->getPlacedTime()) {
          $transition = $order->getState()->getWorkflow()->getTransition('place');
          $order->getState()->applyTransition($transition);
          $order->save();
        }
      }
      elseif (empty($response['transaction']['status']) || $response['transaction']['status'] == 'pending') {
        $expireTime = !empty($this->config->get('minutes_between_validations')) ? $this->config->get('minutes_between_validations') : 5;
        $expireTime *= 30;
        $payment->setState('pending');
        $time = REQUEST_TIME + $expireTime;
        $payment->setExpiresTime($time);
        $payment->setRemoteState($response['transaction']['status']);
        $payment->save();
      }
      else {
        $payment->setState('voided');
        $payment->setRemoteState($response['transaction']['status']);
        $payment->save();
      }
    }
  }

  /**
   * Return a random status if test.
   */
  protected function testBankTransfer() {
    $statues = ['pending', 'approved', 'rejected'];
    $status = array_rand($statues);
    return [
      'error' => FALSE,
      'transaction' => [
        'status' => $statues[$status],
      ],
    ];
  }

}

<?php

namespace Drupal\commerce_globalpay_redeban\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\commerce_globalpay_redeban\GlobalPayRedebanUtilitiesInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\Core\Url;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Colombia Points.
 */
class PseForm extends BasePaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * The block plugin.
   *
   * @var \Drupal\Core\Block\BlockPluginInterface
   */
  protected $plugin;

  /**
   * Request stack that controls the lifecycle of requests.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Defines an account interface which represents the current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The GlobalPay Utilities.
   *
   * @var \Drupal\commerce_globalpay_redeban\GlobalPayRedebanUtilitiesInterface
   */
  protected $globalPayUtilities;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * SystemBrandingOffCanvasForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\commerce_globalpay_redeban\GlobalPayRedebanUtilitiesInterface $globalpay_utilities
   *   The globalpay utilities.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack that controls the lifecycle of requests.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    AccountInterface $current_user,
    GlobalPayRedebanUtilitiesInterface $globalpay_utilities,
    RequestStack $request_stack,
    MessengerInterface $messenger,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
    $this->globalPayUtilities = $globalpay_utilities;
    $this->requestStack = $request_stack->getCurrentRequest();
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('commerce_globalpay_redeban.utilities'),
      $container->get('request_stack'),
      $container->get('messenger'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // Replacement for parent form.
    if (empty($form['#return_url'])) {
      throw new \InvalidArgumentException('The offsite-payment form requires the #return_url property.');
    }
    if (empty($form['#cancel_url'])) {
      throw new \InvalidArgumentException('The offsite-payment form requires the #cancel_url property.');
    }
    if (!isset($form['#capture'])) {
      $form['#capture'] = TRUE;
    }

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->getEntity();
    $order = $payment->getOrder();
    $gateway = $payment->getPaymentGateway();
    $gatewayConfiguration = $gateway->getPluginConfiguration();

    $hash = $this->globalPayUtilities->generateHash($gatewayConfiguration['api_login'], $gatewayConfiguration['api_key']);
    $banksList = $this->globalPayUtilities->getBanks($gatewayConfiguration['endpoint_banks'], 'PSE', $hash);

    if (empty($banksList['error'])) {
      $bankOptions = [];
      foreach ($banksList['banks'] as $bank) {
        $bankOptions[$bank['code']] = $bank['name'];
      }

      $form['#prefix'] = '<div id="globalpay-payment-ajax">';
      $form['#suffix'] = '</div>';

      $form['user_type'] = [
        '#type' => 'select',
        '#title' => $this->t('Type of person'),
        '#empty_option' => $this->t('- Select -'),
        '#options' => [
          'N' => $this->t('Natural person'),
          'J' => $this->t('Legal person'),
        ],
        '#required' => TRUE,
      ];

      $form['user_type_fis_num'] = [
        '#type' => 'select',
        '#title' => $this->t('Identification type'),
        '#empty_option' => $this->t('- Select -'),
        '#options' => [
          'CC' => $this->t('Identification Number'),
          'CE' => $this->t('Inmigration Identification Number'),
          'NIT' => $this->t('Tax identification number'),
          'TI' => $this->t('Identity card'),
          'PP' => $this->t('Passport'),
          'IDC' => $this->t("Unique customer identifier, in the case of unique ID's of customers/users of public services"),
          'RC' => $this->t('Birth certificate'),
          'CEL' => $this->t('(*) Mobile line'),
          'DE' => $this->t('Foreign identification document'),
        ],
        '#required' => TRUE,
      ];

      $form['user_fiscal_number'] = [
        '#type' => 'tel',
        '#title' => $this->t('Identification number'),
        '#required' => TRUE,
      ];

      $form['user_name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Username'),
        '#required' => TRUE,
      ];

      $form['bank_code'] = [
        '#type' => 'select',
        '#options' => $bankOptions,
        '#title' => $this->t('Bank'),
        '#required' => TRUE,
      ];

      if ($gatewayConfiguration['mode'] == 'test') {
        $form['dev_reference'] = [
          '#type' => 'select',
          '#title' => $this->t('DEV Reference'),
          '#empty_option' => $this->t('- Select -'),
          '#options' => [
            'pending' => $this->t('Pending'),
            'rejected' => $this->t('Rejected'),
            'failure' => $this->t('Failure'),
            'approved' => $this->t('Aproved'),
          ],
          '#required' => FALSE,
        ];
      }
      else {
        $form['dev_reference'] = [
          '#type' => 'value',
          '#value' => $order->id(),
        ];
      }

      $form['actions'] = [
        '#type' => 'actions',
      ];

      $form['actions']['cancel'] = [
        '#type' => 'submit',
        '#value' => $this->t('Cancel'),
        '#submit' => [[$this, 'cancelTransferBankUrl']],
        '#limit_validation_errors' => [],
      ];

      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Continue'),
        '#submit' => [[$this, 'submitTransferBankUrl']],
      ];
    }
    else {
      $form['error'] = [
        '#markup' => $this->t("Can't get banks list from GlobalPay by Redeban."),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function cancelTransferBankUrl(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->getEntity();
    $order = $payment->getOrder();
    $this->messenger->addMessage($this->t('Payment canceled by user.'));
    throw new NeedsRedirectException($this->buildCancelUrl($order)->toString());
  }

  /**
   * {@inheritdoc}
   */
  public function submitTransferBankUrl(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->getEntity();
    $order = $payment->getOrder();
    $orderTotal = $order->getTotalPrice();
    $userData = $form_state->getValues()['payment_process']['offsite_payment'];
    $gateway = $payment->getPaymentGateway();
    $gatewayConfiguration = $gateway->getPluginConfiguration();
    $hash = $this->globalPayUtilities->generateHash($gatewayConfiguration['api_login'], $gatewayConfiguration['api_key']);

    if ($gatewayConfiguration['mode'] == 'test') {
      $userData['bank_code'] = '1022';
    }
    $data = [
      "carrier" => [
        "id" => "PSE",
        "extra_params" => [
          "bank_code" => $userData['bank_code'],
          "response_url" => $this->buildReturnUrl($order)->toString(),
          "user" => [
            "name" => $userData['user_name'],
            "fiscal_number" => $userData['user_fiscal_number'],
            "type" => $userData['user_type'],
            "type_fis_number" => $userData['user_type_fis_num'],
            "ip_address" => $order->getIpAddress(),
          ],
        ],
      ],
      "user" => [
        "id" => $order->getCustomerId(),
        "email" => $order->getEmail(),
      ],
      "order" => [
        "country" => "COL",
        'dev_reference' => $order->id(),
        "currency" => $orderTotal->getCurrencyCode(),
        "amount" => (float) $orderTotal->getNumber(),
        "vat" => (float) $this->calculateTax($order),
        "description" => $order->bundle(),
      ],
    ];
    if (!empty($userData['dev_reference'])) {
      $data['order']['dev_reference'] = $userData['dev_reference'];
    }
    $response = $this->globalPayUtilities->createBankTransfer($gatewayConfiguration['endpoint_create_transaction'], $hash, $data);
    if (!empty($response['transaction'])) {
      // Create a payment regardless.
      $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
      $payment = $payment_storage->create([
        'state' => 'pending',
        'amount' => $order->getTotalPrice(),
        'payment_gateway' => $gateway->id(),
        'order_id' => $order->id(),
        'remote_id' => $response['transaction']['id'],
        'remote_state' => $response['transaction']['status_bank'],
      ]);
      $payment->save();
      $data = [];
      if (!empty($response['transaction']['bank_url'])) {
        $redirect_url = Url::fromUri($response['transaction']['bank_url'], [
          'absolute' => TRUE,
          'query' => $data,
        ])->toString();
        throw new NeedsRedirectException($redirect_url);
      }
    }
    $this->messenger->addMessage($this->t('Failed to get the bank url for the payment.'));
    throw new NeedsRedirectException($this->buildCancelUrl($order)->toString());
  }

  /**
   * Calculate the TAX.
   *
   * @param Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return float
   *   The calculated tax.
   */
  protected function calculateTax(OrderInterface $order) {
    $tax_amount = 0;
    foreach ($order->getItems() as $order_item) {
      foreach ($order_item->getAdjustments() as $adjustment) {
        if ($adjustment->getType() == 'tax') {
          $tax_amount += $adjustment->getAmount()->getNumber();
        }
      }
    }
    return $tax_amount;
  }

  /**
   * Builds the URL to the "return" page.
   *
   * @param Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return \Drupal\Core\Url
   *   The "return" page URL.
   */
  protected function buildReturnUrl(OrderInterface $order) {
    return Url::fromRoute('commerce_payment.checkout.return', [
      'commerce_order' => $order->id(),
      'step' => 'payment',
    ], ['absolute' => TRUE]);
  }

  /**
   * Builds the URL to the "cancel" page.
   *
   * @param Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return \Drupal\Core\Url
   *   The "cancel" page URL.
   */
  protected function buildCancelUrl(OrderInterface $order) {
    return Url::fromRoute('commerce_payment.checkout.cancel', [
      'commerce_order' => $order->id(),
      'step' => 'payment',
    ], ['absolute' => TRUE]);
  }

}

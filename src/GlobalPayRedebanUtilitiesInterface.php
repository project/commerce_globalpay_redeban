<?php

namespace Drupal\commerce_globalpay_redeban;

/**
 * Class CurrentScityEntity.
 */
interface GlobalPayRedebanUtilitiesInterface {

  /**
   * Calculates hash from components.
   */
  public function generateHash($server_application_code, $server_app_key);

  /**
   * Obtain the banks from globalpay redeban.
   */
  public function getBanks($endpoint, $carrier, $auth_token);

  /**
   * Create bank transfer for PSE method.
   */
  public function createBankTransfer($endpoint, $auth_token, $transfer_data);

  /**
   * Create bank transfer for PSE method.
   */
  public function checkBankTransfer($endpoint, $carrier, $transfer_id, $auth_token);

}

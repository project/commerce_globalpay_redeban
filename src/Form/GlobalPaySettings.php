<?php

namespace Drupal\commerce_globalpay_redeban\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class RechPack WS Settings form.
 */
class GlobalPaySettings extends ConfigFormBase {

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->setConfigFactory($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'commerce_globalpay_redeban.globalpay_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'globalpay_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('commerce_globalpay_redeban.globalpay_settings');
    $form['active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#default_value' => $config->get('active'),
      '#description' => $this->t('If active the system must verify on each cron run the state of payments.'),
    ];

    $form['minutes_between_validations'] = [
      '#type' => 'number',
      '#title' => $this->t('Time between validations of same payment (minutes)'),
      '#default_value' => $config->get('minutes_between_validations') ? $config->get('minutes_between_validations') : 5,
      '#field_suffix' => $this->t('minutes'),
      '#description' => $this->t('If a payment is pending of approval we must check it on each run cron if it is completed or voided. This is the time to wait between each call the web service. By default 5 minutes.'),
    ];

    $form['max_time_validation'] = [
      '#type' => 'number',
      '#title' => $this->t('How many time a payment must be checked for validation (hours)'),
      '#default_value' => $config->get('max_time_validation') ? $config->get('max_time_validation') : 72,
      '#field_suffix' => $this->t('hours'),
      '#description' => $this->t('If a payment is being checked for example three days the system must stop to check it. By default three days (72 hours).'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('commerce_globalpay_redeban.globalpay_settings');
    $form_state->cleanValues();
    $config->setData($form_state->getValues());
    $config->save();
    parent::submitForm($form, $form_state);
  }

}

<?php

namespace Drupal\commerce_globalpay_redeban;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Component\Serialization\Json;
use GuzzleHttp\Exception\ConnectException;

/**
 * Class GlobalPay Redeban utilities.
 */
class GlobalPayRedebanUtilities implements GlobalPayRedebanUtilitiesInterface {

  use StringTranslationTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $httpClientFactory;

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  /**
   * Constructs a new CurrentScityEntity object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Http\ClientFactory $http_client_factory
   *   Http client to make http calls.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   *   Log system.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ClientFactory $http_client_factory,
    LoggerChannelFactory $logger
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->httpClientFactory = $http_client_factory;
    $this->logger = $logger->get('globalpay_redeban');
  }

  /**
   * Calculates hash from components.
   */
  public function generateHash($server_application_code, $server_app_key) {
    $date = new \DateTime();
    $unix_timestamp = $date->getTimestamp();
    $uniq_token_string = $server_app_key . $unix_timestamp;
    $uniq_token_hash = hash('sha256', $uniq_token_string);
    $auth_token = base64_encode($server_application_code . ";" . $unix_timestamp . ";" . $uniq_token_hash);
    return $auth_token;
  }

  /**
   * Obtain the banks from globalpay redeban.
   */
  public function getBanks($endpoint, $carrier, $auth_token) {
    if (empty($endpoint)) {
      return ['error' => TRUE];
    }
    try {
      $url = rtrim($endpoint, '/');
      $clientOptions = [
        'verify' => TRUE,
        'http_errors' => FALSE,
        'connect_timeout' => 30,
      ];
      $client = $this->httpClientFactory->fromOptions($clientOptions);
      $url = $url . '/' . $carrier . '/';
      $headers = [
        'Auth-Token' => $auth_token,
        'Content-Type' => 'application/json',
        'Accept' => 'application/json',
      ];
      $response = $client->get($url, [
        'headers' => $headers,
      ]);

      if ($response->getStatusCode() == 200) {
        return Json::decode($response->getBody());
      }
      else {
        $this->logger->error(
          $this->t(
            "GlobalPay could not get bank list.<br />%reason",
            [
              '%reason' => $response->getReasonPhrase(),
            ],
          )
        );
        return ['error' => TRUE];
      }
    }
    catch (ConnectException $e) {
      watchdog_exception('globalpay_redeban', $e);
      return ['error' => TRUE];
    }
    return ['error' => TRUE];
  }

  /**
   * Create bank transfer for PSE method.
   */
  public function createBankTransfer($endpoint, $auth_token, $transfer_data) {
    if (empty($endpoint)) {
      return ['error' => TRUE];
    }

    try {
      $url = rtrim($endpoint, '/') . '/';
      $clientOptions = [
        'verify' => TRUE,
        'http_errors' => FALSE,
        'connect_timeout' => 30,
      ];
      $client = $this->httpClientFactory->fromOptions($clientOptions);
      $headers = [
        'Auth-Token' => $auth_token,
        'Content-Type' => 'application/json',
        'Accept' => 'application/json',
      ];

      $response = $client->post($url, [
        'headers' => $headers,
        'body' => Json::encode($transfer_data),
      ]);

      if ($response->getStatusCode() == 200) {
        return Json::decode($response->getBody());
      }
      else {
        $this->logger->error(
          $this->t(
            "GlobalPay could not create the transfer.<br />%reason",
            [
              '%reason' => $response->getReasonPhrase(),
            ],
          )
        );
        return ['error' => TRUE];
      }
    }
    catch (ConnectException $e) {
      watchdog_exception('globalpay_redeban', $e);
      return ['error' => TRUE];
    }
    return ['error' => TRUE];
  }

  /**
   * Create bank transfer for PSE method.
   */
  public function checkBankTransfer($endpoint, $carrier, $transfer_id, $auth_token) {
    if (empty($endpoint)) {
      return ['error' => TRUE];
    }

    try {
      $url = rtrim($endpoint, '/');
      $clientOptions = [
        'verify' => TRUE,
        'http_errors' => FALSE,
        'connect_timeout' => 30,
      ];
      $client = $this->httpClientFactory->fromOptions($clientOptions);
      $headers = [
        'Auth-Token' => $auth_token,
        'Content-Type' => 'application/json',
      ];
      $url = $url . '/' . $carrier . '/order/' . $transfer_id . '/';
      $response = $client->get($url, [
        'headers' => $headers,
      ]);

      if ($response->getStatusCode() == 200) {
        return Json::decode($response->getBody());
      }
      else {
        $this->logger->error(
          $this->t(
            "GlobalPay could check the transfer.<br />%reason",
            [
              '%reason' => $response->getReasonPhrase(),
            ],
          )
        );
        return ['error' => TRUE];
      }
    }
    catch (ConnectException $e) {
      watchdog_exception('globalpay_redeban', $e);
      return ['error' => TRUE];
    }
    return ['error' => TRUE];
  }

}
